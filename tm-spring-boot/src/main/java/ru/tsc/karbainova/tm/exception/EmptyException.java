package ru.tsc.karbainova.tm.exception;

import org.jetbrains.annotations.NotNull;

public class EmptyException extends AbstractException{
    @NotNull
    public EmptyException() {
        super("Error. Field is empty");
    }
}
